﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ApiTwo.Controllers {
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase {
        [HttpGet]
        public IEnumerable<string> Get() {
            return new string[] {"value1", "value2"};
        }

        [HttpGet("{id}")]
        public string Get(int id) {
            return $"Get：{id}";
        }

        [HttpPost]
        public string Post([FromForm] string value) {
            return $"Post:{value}";
        }

        [HttpPut("{id}")]
        public string Put(int id, [FromForm] string value) {
            return $"Put:{id}:{value}";
        }

        [HttpDelete("{id}")]
        public string Delete(int id) {
            return $"Delete:{id}";
        }
    }
}