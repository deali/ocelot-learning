# 使用Ocelot实现Api网关

## 网关

微服务时代，一个项目通常由很多个不同的独立服务组成，但是提供服务只需要一个统一的地址暴露在公网提供访问入口。在.Net Core中我们可以使用Ocelot来搭建网关服务，配置什么的也很简单方便。

`Ocelot`的功能远非常强大，除了转发请求之外，还可以实现请求聚合、服务发现、认证、鉴权、限流熔断等功能，并内置了负载均衡器，而且这些功能都是只需要简单的配置即可完成。

## 开发测试服务

创建两个WebApi项目，这里我创建了`ApiOne`和`ApiTwo`，修改一下默认的`WeatherForecast.cs`，增加一个`Source`字段，以便于分辨数据来自哪个接口。

代码如下：

```c#
namespace ApiOne {
    public class WeatherForecast {
        public string Source { get; set; } = "ApiOne";
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int) (TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
```

```c#
namespace ApiTwo {
    public class WeatherForecast {
        public string Source { get; set; } = "ApiTwo";
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int) (TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
```

## 添加Swagger

为了方便我们看到添加的接口，可以添加Swagger查看接口文档。

首先NuGet安装`Swashbuckle.AspNetCore`

配置项目输出xml文档，编辑`ApiOne.csproj`，添加以下代码：

```xml
<PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
    <DocumentationFile>bin\Debug\netcoreapp3.1\Api.xml</DocumentationFile>
</PropertyGroup>
```

接着在`Startup.cs`中注册Swagger服务，代码如下：

```c#
public void ConfigureServices(IServiceCollection services) {
    services.AddControllers();

    // 配置Swagger
    services.AddSwaggerGen(options => {
        options.SwaggerDoc("v1",
            new OpenApiInfo {
                Version = "v1",
                Title = "ApiOne",
            }
        );

        // 为 Swagger JSON and UI设置xml文档注释路径
        // 获取应用程序所在目录（绝对，不受工作目录影响，建议采用此方法获取路径）
        var basePath = Path.GetDirectoryName(typeof(Program).Assembly.Location);
        var xmlPath = Path.Combine(basePath!, "Api.xml");

        options.IncludeXmlComments(xmlPath);
    });
}
```

```c#
public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
    if (env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
    }

    app.UseHttpsRedirection();

    app.UseRouting();

    app.UseAuthorization();

    app.UseSwagger();

    app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "OracleDataApi v1"));

    app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
}
```

现在运行`ApiOne`之后访问`localhost:5000/swagger`就可以看到接口文档了~

`ApiTwo`项目的操作和这个类似，不重复了。

## 运行测试服务

编辑`ApiOne`和`ApiTwo`的启动配置文件`launchSettings.json`，修改一下启动的端口。

```json
"ApiOne": {
  "commandName": "Project",
  "launchBrowser": false,
  "launchUrl": "swagger",
  "applicationUrl": "http://localhost:5051",
  "environmentVariables": {
    "ASPNETCORE_ENVIRONMENT": "Development"
  }
}
```

```json
"ApiTwo": {
  "commandName": "Project",
  "launchBrowser": false,
  "launchUrl": "swagger",
  "applicationUrl": "http://localhost:5052",
  "environmentVariables": {
    "ASPNETCORE_ENVIRONMENT": "Development"
  }
}
```

然后启动这俩项目~

## 添加网关项目

添加一个空的Asp.Net Core项目，我这里命名为`ApiGateway`，项目目录下添加配置文件`ocelot.json`，内容如下：

```json
{
  "Routes": [
    {
      "DownstreamPathTemplate": "/WeatherForecast",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "localhost",
          "Port": 5051
        }
      ],
      "UpstreamPathTemplate": "/ApiOne/WeatherForecast",
      "UpstreamHttpMethod": [
        "Get"
      ]
    },
    {
      "DownstreamPathTemplate": "/ApiOne",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "localhost",
          "Port": 5051
        }
      ],
      "UpstreamPathTemplate": "/ApiOne",
      "UpstreamHttpMethod": [
        "Get",
        "POST"
      ]
    },
    {
      "DownstreamPathTemplate": "/ApiOne/{id}",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "localhost",
          "Port": 5051
        }
      ],
      "UpstreamPathTemplate": "/ApiOne/{id}",
      "UpstreamHttpMethod": [
        "Get",
        "Put",
        "Delete"
      ]
    },
    {
      "DownstreamPathTemplate": "/WeatherForecast",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "localhost",
          "Port": 5052
        }
      ],
      "UpstreamPathTemplate": "/ApiTwo/WeatherForecast",
      "UpstreamHttpMethod": [
        "Get"
      ]
    },
    {
      "DownstreamPathTemplate": "/ApiTwo",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "localhost",
          "Port": 5052
        }
      ],
      "UpstreamPathTemplate": "/ApiTwo",
      "UpstreamHttpMethod": [
        "Get",
        "POST"
      ]
    },
    {
      "DownstreamPathTemplate": "/ApiTwo/{id}",
      "DownstreamScheme": "http",
      "DownstreamHostAndPorts": [
        {
          "Host": "localhost",
          "Port": 5052
        }
      ],
      "UpstreamPathTemplate": "/ApiTwo/{id}",
      "UpstreamHttpMethod": [
        "Get",
        "Put",
        "Delete"
      ]
    }
  ],
  "GlobalConfiguration": {
    "BaseUrl": "http://localhost:44335"
  }
}
```

主要就是将DownstreamPathTemplate模板内容转换为UpstreamPathTemplate模板内容进行接口的访问，同时可以指定HTTP请求的方式。GlobalConfiguration中的BaseUrl就是暴漏出去的网关地址。

配置好`ocelot.json`之后要在项目中进一步配置。

首先配置`Program.cs`：

```c#
public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureAppConfiguration((context, builder) =>
            builder.AddJsonFile("ocelot.json", optional: false, reloadOnChange: true)
        )
        .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
```

继续配置`Startup.cs`：

```c#
public void ConfigureServices(IServiceCollection services) {
    services.AddOcelot();
}

public void Configure(IApplicationBuilder app,
    IWebHostEnvironment env) {
    if (env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
    }

    app.UseRouting();

    app.UseEndpoints(endpoints => {
        endpoints.MapGet("/", async context => {
            await context.Response.WriteAsync("Hello World!");
        });
    });

    // 使用 Ocelot
    app.UseOcelot().Wait();
}
```

完成上述操作之后，运行`ApiGateway`项目，假设`ApiGateway`项目的地址是`localhost:5000`，那么我们要访问`ApiOne`的`WeatherForecast`功能的地址则是：`localhost:5000/ApiOne/WeatherForecast`，即`ocelot.json`文件中配置的地址映射。

## 其他网关服务

- ProxyKit
- 微软新发布的YARP

## 参考资料

关于`Ocelot`的更多功能可以查看官方文档和实例~

- Ocelot官网：https://threemammals.com/ocelot
- Ocelot文档：https://ocelot.readthedocs.io
- GitHub：https://github.com/ThreeMammals/Ocelot
- Ocelot资源汇总：https://www.cnblogs.com/shanyou/p/10363360.html

